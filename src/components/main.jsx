import React from "react";

import FilterComponent from "./shared/filterComponent";
import CardComponent from "./cardComponent";

// import { Asset2, Asset3, Asset4, Asset5 } from "../resources";
import Asset2 from "../resources/Asset2.png";
import Asset3 from "../resources/Asset3.png";
import Asset4 from "../resources/Asset4.png";
import Asset5 from "../resources/Asset5.png";

const information = [
  {
    image: Asset3,
    theme: "MICROLECCION",
    title: "Lorem ipsum dolor",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet...",
    tags: ["Etiqueta 1"],
  },
  {
    image: Asset2,
    theme: "MICROLECCION",
    title: "Lorem ipsum dolor",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet...",
    tags: ["Etiqueta 1", "Etiqueta 2", "Etiqueta 3"],
  },
  {
    theme: "RECOMENDACIONES",
    title: "In enim justo, rhoncus ut imperdiet venenatis vitae justo",
    description: "Lorem ipsum dolor sit amet, consectetuer...",
    tags: ["Etiqueta 1"],
  },
  {
    image: Asset4,
    theme: "DOCUMENTOS",
    tags: ["Etiqueta 1", "Etiqueta 2", "Etiqueta 3"],
  },
  {
    theme: "DOCUMENTOS",
    title: "",
    tags: ["Etiqueta 1", "Etiqueta 2"],
  },
  {
    image: Asset5,
    theme: "ANIMACION",
    title: "Lorem ipsum dolor",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet...",
    tags: ["Etiqueta 1", "Etiqueta 2", "Etiqueta 3"],
  },
];

export default (props) => {
  return (
    <div className="container">
      <FilterComponent />
      <div class="row">
        {information.map((item) => (
          <CardComponent
            data={item}
            className="col-4 col-sm-12 col-md-6 col-xl-4"
          />
        ))}
      </div>
    </div>
  );
};
