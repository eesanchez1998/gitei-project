import React from "react";

const itemsToFilter = [
  {
    key: "1",
    name: "Mas Reciente",
  },
  {
    key: "1",
    name: "Menos Reciente",
  },
  {
    key: "1",
    name: "Tipo de Recurso",
  },
];

export default (props) => {
  return (
    <div class="filter-box ">
      <form class="form-inline">
        <div class="dropdown mx-sm-3 mb-2">
          <button
            class="btn dropdown-toggle shadow"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Ordenar Por
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            {itemsToFilter.map((item, key) => (
              <a key={key} class="dropdown-item " href="/#">
                {item.name}
              </a>
            ))}
          </div>
        </div>
        <button type="button" class="btn btn-outline-dark mx-sm-3 mb-2 shadow">
          Filtrar
        </button>
      </form>
      <hr />
    </div>
  );
};
