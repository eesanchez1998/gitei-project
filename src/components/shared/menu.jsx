import React from "react";

import ContextPage from "./context-page";

const menuItems = [
  {
    key: "1",
    name: "Inicio",
  },
  {
    key: "2",
    name: "Contenidos Educativos",
  },
  {
    key: "3",
    name: "Escuela Virtual",
  },
  {
    key: "4",
    name: "Videos",
  },
];

export default (props) => {
  return (
    <ContextPage.Consumer>
      {({ currentPage, setCurrentPage }) => (
        <div className="navbar-content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light border-radius-25">
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarTogglerDemo02"
              aria-controls="navbarTogglerDemo02"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                {menuItems.map((item, key) => (
                  <li key={key} class="nav-item">
                    <a
                      class="nav-link"
                      onClick={() => setCurrentPage(item.name)}
                      href="/#"
                    >
                      {item.name}
                    </a>
                  </li>
                ))}
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <input
                  class="form-control mr-sm-2"
                  type="search"
                  placeholder="Buscar en ANVS"
                />
                <button class="btn btn-outline-secondary my-2 my-sm-0 container-button">
                  <svg
                    class="bi bi-search text-light"
                    width="1em"
                    height="1em"
                    viewBox="0 0 16 16"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"
                    />
                    <path
                      fill-rule="evenodd"
                      d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"
                    />
                  </svg>
                </button>
              </form>
            </div>
          </nav>
        </div>
      )}
    </ContextPage.Consumer>
  );
};
