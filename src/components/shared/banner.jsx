import React, { Fragment } from "react";

import logo from "../../resources/logo.png";

import Menu from "./menu";
import Path from "./path";

export default (props) => {
  return (
    <Fragment>
      <div className="header">
        <img src={logo} alt="" />
        <Menu />
      </div>
      <Path />
    </Fragment>
  );
};
