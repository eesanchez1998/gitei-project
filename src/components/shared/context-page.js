import React from "react";

export const page = {
  currentPage: null,
  setCurrentPage: () => {},
};

export default React.createContext(page);
