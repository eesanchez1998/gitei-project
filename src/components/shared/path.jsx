import React from "react";

import ContextPage from "./context-page";

export default (props) => {
  return (
    <ContextPage.Consumer>
      {({ currentPage, setCurrentPage }) => (
        <div className="path">
          Inicio {currentPage !== null && `> ${currentPage}`}
        </div>
      )}
    </ContextPage.Consumer>
  );
};
