import React from "react";

export default ({ data }) => {
  return (
    <div class="card shadow">
      <div>
        <img src={data.image} alt="" width="100%" />
      </div>
      <div class="card-content">
        <div class="tag-list">
          {data.tags.map((item) => (
            <div class="tag-item">{item}</div>
          ))}
        </div>
        <p style={{ fontSize: 15, marginTop: 10 }}>{data.theme}</p>
        <h4>{data.title}</h4>
        <p>{data.description}</p>
      </div>
    </div>
  );
};
