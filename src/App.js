import React from "react";
import Header from "./components/shared/banner";
import Content from "./components/main";

import ContextPage from "./components/shared/context-page";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: null,
      setCurrentPage: this.setCurrentPage,
    };
  }

  setCurrentPage = (page) => {
    this.setState({ currentPage: page });
  };

  render() {
    return (
      <div className="App">
        <ContextPage.Provider value={this.state}>
          <Header />
          <Content />
        </ContextPage.Provider>
      </div>
    );
  }
}

export default App;
