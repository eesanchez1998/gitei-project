## Informacion del Proyecto

El proyecto fue creado con el framework de JS React, el cual trabaja globalmente con temas acordes a hooks, context, entre algunos otros en relacion a React.
La base de la presentacion de la pagina esta hecha en boostrap y fue utilizado el preprocesador de CSS, Sass, trabajando con mixins, variables, y la fuente requerida que se encontraba en el marco de adobe ilustrator

## Scripts para correr el Proyecto

### `yarn install`

Instala las dependencias que son necesarias para correr el proyecto

### `yarn start`

Corre la aplicacion en modo desarrollo.
Abrir en [http://localhost:3000](http://localhost:3000) para ver en el navegador.

La pagina recargara frente a cualquier cambio.<br />

### `yarn test`

Sirve para la ejecucion de pruebas unitarias

### `yarn build`

Construye el app para produccion.
